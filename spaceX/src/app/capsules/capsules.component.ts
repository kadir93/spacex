import { Component, OnInit } from '@angular/core';
import { SpaceXService } from '../service/space-x.service'
import { Capsule } from '../models/models'
import { Observable } from 'rxjs';


@Component({
  selector: 'app-capsules',
  templateUrl: './capsules.component.html',
  styleUrls: ['./capsules.component.css']
})
export class CapsulesComponent implements OnInit{



  capsuleList: Capsule[] = [];
  displayedColumns: string[] = ['serial','name','status','details']

  constructor(private service: SpaceXService) {
}

 ngOnInit() {
    this.service.getCapsules().subscribe(e => {
            this.capsuleList = e
     });
  }
}
