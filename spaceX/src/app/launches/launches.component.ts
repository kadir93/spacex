import { Component, OnInit } from '@angular/core';
import { SpaceXService } from '../service/space-x.service'
import { Launch } from '../models/models'
import { Observable } from 'rxjs';


@Component({
  selector: 'app-launches',
  templateUrl: './launches.component.html',
  styleUrls: ['./launches.component.css']
})

export class LaunchesComponent implements OnInit{

  launchList: Launch[] = [];
  displayedColumns: string[] = ['id', 'mission', 'date', 'status','img']

  constructor(private service: SpaceXService) {
}

  ngOnInit() {
    this.service.getLaunches().subscribe(e => {
            this.launchList = e
     });
  }

}
