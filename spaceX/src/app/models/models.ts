export class Launch {
  constructor(
          public id: string,
          public mission: string,
          public date: string,
      ){ }
}

export class Capsule {
  constructor(
          public id: string,
          public mission: string,
          public date: string,
      ){ }
}

