import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Launch,Capsule } from '../models/models';


@Injectable({
  providedIn: 'root'
})

export class SpaceXService {

  constructor(private http: HttpClient) { }

  getLaunches(): Observable<Launch[]> {
      return this.http.get<Launch[]>('https://api.spacexdata.com/v3/launches')
    }

  getCapsules(): Observable<Capsule[]> {
        return this.http.get<Launch[]>('https://api.spacexdata.com/v3/capsules')
      }
}
